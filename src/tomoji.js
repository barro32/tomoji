const LETTER_WIDTH = 5
const LETTER_HEIGHT = 5

const emoji = '🌟'
const space = '🔵'

function handleChange() {
  const emoji = document.querySelector('.foreground').value
  const space = document.querySelector('.background').value
  const word = document.querySelector('.input').value.toUpperCase()

  if(validChars(word)) {
    let output = space

    for (let i = 0; i < LETTER_HEIGHT; i++) {
        for (let letter of word.toLocaleUpperCase()) {

            let letterLineArray = alphabet[letter].substring(LETTER_WIDTH * i, LETTER_WIDTH * (i + 1)).split('')

            let emojiLine = letterLineArray.map(bit => parseInt(bit, 10) ? emoji : space).join('')
            output = `${output}${emojiLine}${space}`
        }
        if(i < LETTER_HEIGHT - 1) {
            output = `${output}\n${space}`
        } else {
            output = `${output}\n`
        }
    }
    document.querySelector('.output').textContent = output
  }
}

const validChars = (word) => {
    for(let l of word) {
        if(typeof alphabet[l] !== 'string') {
            console.error(`"${l}" not supported. You could add it to alphabet.js and make a merge request!`)
            return false
        }
    }
    return true
}

const alphabet = {
    'A': '0111010001111111000110001',
    'B': '1111010001111101000111110',
    'C': '0111010001100001000101110',
    'D': '1111010001100011000111110',
    'E': '1111110000111101000011111',
    'F': '1111110000111101000010000',
    'G': '0111110000100111000101110',
    'H': '1000110001111111000110001',
    'I': '1111100100001000010011111',
    'J': '1111100010000101001001100',
    'K': '1000110010111001001010001',
    'L': '1000010000100001000011111',
    'M': '1000111011101011000110001',
    'N': '1000111001101011001110001',
    'O': '0111010001100011000101110',
    'P': '1111010001111101000010000',
    'Q': '0111010001100011001101111',
    'R': '1111010001111101000110001',
    'S': '0111110000011100000111110',
    'T': '1111100100001000010000100',
    'U': '1000110001100011000101110',
    'V': '1000110001010100101000100',
    'W': '1000110001101011010101010',
    'X': '1000101010001000101010001',
    'Y': '1000101010001000010000100',
    'Z': '1111100010001000100011111',
    '0': '0111010001100011000101110',
    '1': '0010001100001000010001110',
    '4': '0011001010100101111100010',
    ' ': '0000000000000000000000000',
    '+': '0010000100111110010000100',
    '-': '0000000000111110000000000',
    '&': '0010001010001000101001101',
    '?': '0111010001000100010000100',
}
